package com.smef.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.smef.entity.Region;

public class RegionDB extends Database {

    public RegionDB() {
        super();
    }


    public List<Region> getRegions() {
        List<Region> regions = new ArrayList<>();
        String sql = "SELECT * FROM region";
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Region r = new Region();
                r.setId(rs.getInt(1));
                r.setParentId(rs.getInt(2));
                r.setExctractionTimeStamp(rs.getTimestamp(3).getTime());
                r.setLanguage(rs.getInt(4));
                r.setType(rs.getInt(5));
                r.setName(rs.getString(6));
                r.setProjectId(rs.getInt(7));

                regions.add(r);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return regions;
    }

    public List<Region> getRegionsByIDs(int[] id) {
        List<Region> regions = new ArrayList<>();
        for (int i : id) {
            List<Region> current = getRegionsByParametres(0, 0, 0, null, i, null, 0);
            regions.addAll(current);
        }
        return regions;
    }

    public List<Region> getRegionsByNames(String[] id) {
        List<Region> regions = new ArrayList<>();
        for (String i : id) {
            List<Region> current = getRegionsByParametres(0, 0, 0, i, 0, null, 0);
            regions.addAll(current);
        }
        return regions;
    }

    public List<Region> getRegionsByTypes(int[] id) {
        List<Region> regions = new ArrayList<>();
        for (int i : id) {
            List<Region> current = getRegionsByParametres(0, 0, i, null, 0, null, 0);
            regions.addAll(current);
        }
        return regions;
    }

    public List<Region> getRegionsByCommitIDs(String[] id) {
        List<Region> regions = new ArrayList<>();
        for (String i : id) {
            List<Region> current = getRegionsByParametres(0, 0, 0, null, 0, i, 0);
            regions.addAll(current);
        }
        return regions;
    }

    public List<Region> getRegionsByCommitTSs(long[] id) {
        List<Region> regions = new ArrayList<>();
        for (long i : id) {
            List<Region> current = getRegionsByParametres(0, 0, 0, null, 0, null, i);
            regions.addAll(current);
        }
        return regions;
    }

    public Region getRegionById(int id) {
        Region r = new Region();
        String sql = "SELECT * FROM region WHERE id=" + id;
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                r.setId(rs.getInt(1));
                r.setParentId(rs.getInt(2));
                System.out.println(rs.getTimestamp(3));
                r.setExctractionTimeStamp(rs.getTimestamp(3).getTime());
                r.setLanguage(rs.getInt(4));
                r.setType(rs.getInt(5));
                r.setName(rs.getString(6));
                r.setProjectId(rs.getInt(7));
                System.out.println(r.getExctractionTimeStamp());
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return r;
    }

    public List<Region> getRegionByCommitId(int id) {
        List<Region> regions = new ArrayList<>();
        String sql = "SELECT * FROM region WHERE commit_id=" + id;
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Region r = new Region();
                r.setId(rs.getInt(1));
                r.setParentId(rs.getInt(2));
                r.setExctractionTimeStamp(rs.getTimestamp(3).getTime());
                r.setLanguage(rs.getInt(4));
                r.setType(rs.getInt(5));
                r.setName(rs.getString(6));
                r.setProjectId(rs.getInt(7));

                regions.add(r);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return regions;
    }

    public List<Region> getRegionBycommitTs(long ts) {
        List<Region> regions = new ArrayList<>();
        String sql = "SELECT * FROM region WHERE commit_ts=" + new Timestamp(ts);
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Region r = new Region();
                r.setId(rs.getInt(1));
                r.setParentId(rs.getInt(2));
                r.setExctractionTimeStamp(rs.getTimestamp(3).getTime());
                r.setLanguage(rs.getInt(4));
                r.setType(rs.getInt(5));
                r.setName(rs.getString(6));
                r.setProjectId(rs.getInt(7));

                regions.add(r);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return regions;
    }

    public List<Region> getRegionByName(String name) {
        List<Region> regions = new ArrayList<>();
        String sql = "SELECT * FROM region WHERE name='" + name + "'";
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Region r = new Region();
                r.setId(rs.getInt(1));
                r.setParentId(rs.getInt(2));
                r.setExctractionTimeStamp(rs.getTimestamp(3).getTime());
                r.setLanguage(rs.getInt(4));
                r.setType(rs.getInt(5));
                r.setName(rs.getString(6));
                r.setProjectId(rs.getInt(7));

                regions.add(r);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return regions;
    }

    public List<Region> getRegionByType(int type) {
        List<Region> regions = new ArrayList<>();
        String sql = "SELECT * FROM region WHERE type=" + type;
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Region r = new Region();
                r.setId(rs.getInt(1));
                r.setParentId(rs.getInt(2));
                r.setExctractionTimeStamp(rs.getTimestamp(3).getTime());
                r.setLanguage(rs.getInt(4));
                r.setType(rs.getInt(5));
                r.setName(rs.getString(6));
                r.setProjectId(rs.getInt(7));

                regions.add(r);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return regions;
    }

    public Region postRegion(Region r) {
        String sql = "INSERT INTO region VALUES (?,?,?,?,?,?,?)";
        System.out.println("ky");

        try {
            PreparedStatement st = getConnection().prepareStatement(sql);
            st.setInt(1, r.getId());
            st.setInt(2, r.getParentId());
            st.setTimestamp(3, new Timestamp(r.getExctractionTimeStamp()));
            st.setInt(4, r.getLanguage());
            st.setInt(5, r.getType());
            st.setString(6, r.getName());
            st.setInt(7, r.getProjectId());
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }

        return r;
    }

    public List<Region> getRegionsByParametresWithParentId(int parentId, Timestamp extractionts, int language, int type,
                                                           String name, int projectId, int commitId, Timestamp commitTs) {
        List<Region> regions = new ArrayList<>();
        System.out.println(extractionts);
        String sql = "SELECT * FROM region WHERE parent_id=" + parentId + " AND";
        if (extractionts != null)
            sql += " extraction_time_stamp=" + extractionts + " AND";
        if (language != 0)
            sql += " language=" + language + " AND";
        if (type != 0)
            sql += " type=" + type + " AND";
        if (name != null)
            sql += " name='" + name + "' AND";
        if (projectId != 0)
            sql += " project_id=" + projectId + " AND";
        if (commitId != 0)
            sql += " commit_id=" + commitId + " AND";
        if (commitTs != null)
            sql += " commit_ts=" + commitTs + " AND";
        sql = sql.substring(0, sql.length() - 3);

        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Region r = new Region();
                r.setId(rs.getInt(1));
                r.setParentId(rs.getInt(2));
                r.setExctractionTimeStamp(rs.getTimestamp(3).getTime());
                r.setLanguage(rs.getInt(4));
                r.setType(rs.getInt(5));
                r.setName(rs.getString(6));
                r.setProjectId(rs.getInt(7));

                regions.add(r);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return regions;
    }

    public List<Region> getRegionsByParametres(long extractionts, int language, int type, String name,
                                               int projectId, String commitId, long commitTs) {
        List<Region> regions = new ArrayList<>();
        String sql = "SELECT * FROM region WHERE";
        if (extractionts != 0)
            sql += " extraction_time_stamp=" + extractionts + " AND";
        if (language != 0)
            sql += " language=" + language + " AND";
        if (type != 0)
            sql += " type=" + type + " AND";
        if (name != null)
            sql += " name='" + name + "' AND";
        if (projectId != 0)
            sql += " project_id=" + projectId + " AND";
        if (commitId != null)
            sql += " commit_id=" + commitId + " AND";
        if (commitTs != 0)
            sql += " commit_ts=" + commitTs + " AND";
        sql = sql.substring(0, sql.length() - 3);

        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Region r = new Region();
                r.setId(rs.getInt(1));
                r.setParentId(rs.getInt(2));
                r.setExctractionTimeStamp(rs.getTimestamp(3).getTime());
                r.setLanguage(rs.getInt(4));
                r.setType(rs.getInt(5));
                r.setName(rs.getString(6));
                r.setProjectId(rs.getInt(7));

                regions.add(r);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return regions;
    }

}
